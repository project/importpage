importpage.module README.txt
----------------------------


This module is oriented towards sites that wish to accumulate information
quickly and easily.  For example, sites that reference news articles often
find their online links gone after a short time.  Importing the article
directly into a site will minimize the need to cull broken links.  This module
is also handy for creating books of 'clippings' from across the internet.

ATTENTION: Under U.S. copyright law, and under the license granted by most news
organizations, you are allowed to keep one copy of articles you download for
personal use only.  If you are storing articles from newspapers and magazines,
please be sure make check that you are not violating any laws.  Many political
sites and independent news outlets grant free reprint rights to their stories
if (and only if) you do not alter the original in any way.

Files
  - importpage.module
      the actual module (PHP source code)

  - README.txt (this file)
      general module information

  - INSTALL
      installation/configuration instructions

  - CHANGELOG
      change/release history for this module

  - LICENSE
      the license (GNU General Public License) covering the usage,
      modification, and distribution of this software and its
      accompanying files
